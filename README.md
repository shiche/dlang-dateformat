# Date/Time Formatting Library for D

A date/time library, with C *strftime* function like format strings. Some modifiers is not currenly supported: %G, %E, %O, %V, %+, %N. Windows - formats with no leading zero replaced by leading zero. Everything else is compatible with the *stftime* function.

There are set of 'format' functions. It uses current system locale unless you specify your own. You can format following D types:

- Date
- DateTime
- SysTime

Set of **format** functions gives you full date (Tu 03 Nov 2020 09:22:25 MSK, for example), **formatShortDate...** set gives short version of corresponding type (03.11.2020 09:26:36, for russian locale). One of **format** functions has second argument formatString, so you can use all *strftime* capabilities. See it's documentation for details.

If you need to use specific locale it must be present in OS. Call *initDateformatLocale* function to get *Locale* struct for future calls. Locale support comes form another library *dlocale*.
```auto locale = initDateformatLocale("C");```

Use [ddoc](http://dateformat.dpldocs.info/dateformat.html) documentation for details.

>Note: for Windows it uses old style library because it shipped with current compiler (DMD32 D Compiler v2.094.2-dirty) with LCID locales. So use only locales defined [here](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-lcid/a9eac961-e77d-41a6-90a5-ce1a8b0cdb9c?redirectedfrom=MSDN). But format still conforms *strftime* function.
