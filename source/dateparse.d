/** D date/time parse functions.
*
*    Authors: Vitaly Livshic, shiche@yandex.ru
*    Bugs: Uses C setLocale which uses system localization via dlocale library.
*    License: LGPLv3 
*/
module dateparse;
@safe

import std.datetime;
import dlocale;

import std.array;
import std.string;
import std.conv : parse;
import std.algorithm.searching;

/// Parsing exception.
export class DateParseException : Exception
{
    uint position;
    this(string msg, uint position = 0) { super("%s at position %d.".format( msg, position )); this.position = position; }
}

/// Struct that must hold all parts for date/time.
private struct tm
{
    ubyte day = 255;
    ubyte month = 255;
    uint year = 10000;
    ubyte hour = 255;
    ubyte minute = 255;
    ubyte second = 255;
    uint millisecond = 10000;
    uint microsecond = 10000;
    uint nanosecond = 10000;

    TimeZone timezone = null;
}

/**
* Parse string to SysTime with standard string representation.
* Params:
*   source = string to parse
*   formatString = string in C strftime function format.
*   defaultZone = timezone for date
*   locale = used locale
* Returns:
*   parsed SysTime or throws DateParseException exception if there are any errors.
* Examples:
* --------------------
* auto locale = initDateformatLocale("C");
* ...
* auto date = SysTime(DateTime(2020, 3, 8, 14, 33, 52), UTC());
* assert(parseSysTime("Sun Mar  8 14:33:52 2020 UTC", "%c") == date);
* --------------------
*/
export SysTime parseSysTime(string source, 
    string formatString, 
    immutable TimeZone defaultZone = null, 
    Locale locale = getDefaultLocale)
{
    string parse2(string source, out ubyte value, ref uint spos)
    {
        auto len = source.length > 1 && source[ 1 .. 2 ].isNumeric ? 2 : 1;
        spos += len;
        auto part = source[ 0 .. len ].stripLeft;
        value = parse!ubyte(part);
        return source[ len .. $ ];
    }

    string extractYear(string source, ref uint spos, int dataLen, out uint year)
    {
        if (source.length < dataLen)
            throw new DateParseException("Not enough data to parse year!", spos);
        
        auto part = source[ 0 .. dataLen ];
        if (!isNumeric(part))
            throw new DateParseException(format("Year must contain %d digits!", dataLen), spos);
        year = parse!uint(part);
        spos += dataLen;

        return source[ dataLen .. $ ];
    }

    bool isLetter(immutable char ch)
    {
        return ch > 255 || (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
    }

    string extractPart(string source, ref uint spos, string[] names, string message, out ubyte pos, out string part)
    {
        auto p = source.indexOf(" ");
        if (p < 0)
            p = source.length;
        part = source[ 0 .. p ];

		// Remove non letters
        while (part.length > 0 && !isLetter(part[ part.length - 1 ]))
        {
            p--;
            part = part[ 0 .. $ - 1];
        }

        // Compare with array
        for (pos = 0; pos < names.length; pos++)
            if (names[pos] == part)
            {
                spos += p;
                pos++;
                return source[ p .. $ ];
            }
        // Throw exception if there is no value equals part in the array
        throw new DateParseException(message, spos);
    }

    if (source == null || formatString == null)
        throw new DateParseException("Source or formatString is null");

    formatString = formatString
        .replace("%c", locale.dateTime)
        .replace("%x", locale.date)
        .replace("%X", locale.time)
        .replace("%D","%m/%d/%y")
        .replace("%r", "%I:%M:%S %p")
        .replace("%R", "%H:%M")
        .replace("%T", "%H:%M:%S")
        .replace("%F", "%Y-%m-%d");

    uint spos = 0, fpos = 0;
    tm tm;
    while (source.length > 0 && formatString.length > 0)
    {
        // Ignore non data-character
        if (formatString[0] != '%')
        {
            if (source[0] == formatString[0])
            {
                source = source[ 1 .. $ ];
                formatString = formatString[ 1 .. $ ];
                spos++; fpos++;
                continue;
            }
            else
                throw new DateParseException("Invalid character " ~ source[0], spos);
        }

        // Format symbol detected
        if (formatString.length < 2)
            throw new DateParseException("Invalid format string. % without specifier.", fpos);
        auto fmtChar = formatString[1];
        formatString = formatString[ 2 .. $ ];
        switch (fmtChar)
        {
            case 'a':
            case 'A':
                {
                    // Weekday silently ignored itself, but check it presence.
                    string part;
                    ubyte pos;
                    source = extractPart(source,
                        spos,
                        fmtChar == 'a' ? locale.abbrWeekDays : locale.weekDays,
                        "Invalid week day name",
                        pos,
                        part);
                }
                break;
            case 'b':
            case 'B':
                {
                    // Month name
                    string part;
                    ubyte pos;
                    source = extractPart(source, 
                        spos,  
                        fmtChar == 'b' ? locale.abbrMonthes : locale.monthes,
                        "Invalid month name",
                        pos,
                        part);
                    tm.month = pos;
                }
                break;
            case 'e':
            case 'd':
                source = parse2(source, tm.day, spos);
                break;
            case 'm':
                source = parse2(source, tm.month, spos);
                break;
            case 'Y':
                source = extractYear(source, spos, 4, tm.year);
                break;
            case 'y':
                {
                    source = extractYear(source, spos, 2, tm.year);
                    tm.year += tm.year >= 69 ? 1900 : 2000;
                }
                break;
			case 'I':
            case 'H':
                source = parse2(source, tm.hour, spos);
                break;
            case 'M':
                source = parse2(source, tm.minute, spos);
                break;
            case 'S':
                source = parse2(source, tm.second, spos);
                break;
            case 'z':
                {
                    if (source.length < 3)
                        throw new DateParseException("Cannot read offset for time zone, string too short!", spos);
                    auto part = source[ 0 .. 3 ];
                    spos += 3;
                    source = source[ 0 .. 3 ];
                    auto utcOffset =  parse!int(part);
                    tm.timezone = new SimpleTimeZone(utcOffset.hours);
                }
                break;
            case 'Z':
                {
                    auto p = source.indexOf(' ');
                    if (p < 0)
                        p = source.length;
                    auto part = source[ 0 .. p ];
                    source = source[ p .. $ ];
                    spos += p;
                    version(Posix)
                        tm.timezone = cast(TimeZone)PosixTimeZone.getTimeZone(part);
                    version(Windows)
                        tm.timezone = cast(TimeZone)WindowsTimeZone.getTimeZone(part);
                }
                break; 

            default: throw new DateParseException("Unknown format specifier: %" ~ fmtChar, fpos);
        }
        fpos += 2;
    }

    // Check tm struct
    if (tm.day == 255 || tm.month == 255 || tm.year == 10000)
        throw new DateParseException("Not enough data to parse Date!", 0);
	if (tm.second == 255)
		tm.second = 0;
    if (tm.hour == 255 || tm.minute == 255)
        throw new DateParseException("Not enough data to parse time part!", 0);
    if (tm.timezone is null)
        tm.timezone = cast(TimeZone)defaultZone;

    // If cannot determine parts of second  - it will be zero 
    tm.millisecond = tm.millisecond > 999 ? 0 : tm.millisecond;
    tm.microsecond = tm.microsecond > 999 ? 0 : tm.microsecond;
    tm.nanosecond = tm.nanosecond > 999 ? 0 : tm.nanosecond;

    return SysTime(DateTime(tm.year, tm.month, tm.day, tm.hour, tm.minute, tm.second), cast(immutable TimeZone)tm.timezone);
}

/**
* Parse string to SysTime with standard string representation. Equals parseSysTime(source, "%c", locale).
* Params:
*   source = string to parse
*   defaultZone = default time zone if it cannot be determined.
*   locale = used locale
* Returns:
*   parsed SysTime or throws DateParseException exception if there are any errors.
*/
export SysTime parseSysTime(string source, immutable TimeZone defaultZone = null, Locale locale = getDefaultLocale)
{
    return parseSysTime(source, "%c", defaultZone, locale);
}

unittest
{
    import std.stdio;

    writeln("\nDate parsing test");

    Locale locale;
    version(Posix)
        locale = initDateformatLocale("C");
    else
        version(Windows)
            locale = initDateformatLocale("en");
        else
            locale = initDateformatLocale("en-US");
    setDefaultLocale(locale.name);
    writeln("Test's locale is " ~ locale.name);

    SysTime date;
    string source;

    // Locale specific
	version(Posix)
	{
		date = SysTime(DateTime(2020, 3, 8, 14, 33, 52), UTC());
		source = "Sun Mar  8 14:33:52 2020 UTC";
		assert(parseSysTime(source, "%c", UTC()) == date);
		assert(parseSysTime(source, UTC()) == date);

		assert(parseSysTime("12/07/20 17:45:10", "%x %X", UTC()) == SysTime(DateTime(2020, 12, 7, 17, 45, 10), UTC()));

        // Manual formats
        assert(parseSysTime(source, "%a %b  %d %H:%M:%S %Y %Z") == date); 
        assert(parseSysTime("Sunday March  8 14:33:52 2020 UTC", "%A %B  %d %R:%S %Y %Z") == date);
        
        assert(parseSysTime("03/08/20 14:33:52 UTC", "%D %R:%S %Z") == date); 
        assert(parseSysTime("03/08/20 14:33:52 UTC", "%D %T %Z") == date); 
    
        assert(parseSysTime("03/08/20 14:33:52 UTC", "%m/%d/%y %T %Z") == date);
	}
	version(Windows)
	{
		source = "Monday, December 07, 2020 05:45";
		date = SysTime(DateTime(2020, 12, 7, 5, 45, 0), UTC());
		
		assert(parseSysTime(source, "%c", UTC()) == date);
		assert(parseSysTime(source, UTC()) == date);
		
		// Manual formats
        assert(parseSysTime("Mon, Dec, 07 5:45:0 2020 UTC", "%a, %b, %d %H:%M:%S %Y %Z") == date); 
        assert(parseSysTime("Monday December  07 5:45:00 2020 UTC", "%A %B  %d %R:%S %Y %Z") == date);

        assert(parseSysTime("12/07/20 5:45:0 UTC", "%D %R:%S %Z") == date); 
        assert(parseSysTime("12/07/20 5:45:0 UTC", "%D %T %Z") == date); 
    
        assert(parseSysTime("12/07/20 5:45:0 UTC", "%m/%d/%y %T %Z") == date);
	}

    writeln("Date parsing test finished\n");
}
