/** D date/time format functions.
*
*    Authors: Vitaly Livshic, shiche@yandex.ru
*    Bugs: Uses C setLocale which uses system localization via dlocale library.
*    License: LGPLv3 
*/
module dateformat;
@safe

import std.datetime;
import std.conv;
import std.string : fromStringz;
import std.string : toStringz;
import core.stdc.time;
import core.stdc.locale;
static import std.string;
import std.math : abs;

import dlocale;

private immutable auto startOfTimes = DateTime(1, 1, 1, 0, 0, 0);

/** 
 * Full date.
 * Examples:
 * --------------------
 * format(Clock.currTime()); // Gives Fr 06 Nov 2020 23:17:42 MSK for example
 * --------------------
*/
export string STANDARD_FORMAT = "%c %Z";
/** 
 * Full date without timezone.
 * Examples:
 * --------------------
 * auto dt = DateTime(2020, 11, 6, 23, 17, 42);
 * format(dt); // Gives Fr 06 Nov 2020 23:17:42 for example
 * --------------------
*/
export string STANDARD_FORMAT_WITHOUT_TIMEZONE = "%c";
/// Full date-only, without timezone
version(Windows)
	export string STANDARD_DATE_FORMAT_WITHOUT_TIMEZONE = "%A, %B %d, %Y";	
else
	export string STANDARD_DATE_FORMAT_WITHOUT_TIMEZONE = "%a %d %b %Y";
	
/**
 * Short date format.
 *  Examples:
 * --------------------
 * auto dt = DateTime(2020, 11, 6, 23, 17, 42);
 * formatShortDate(dt); // Gives 06.11.2020 for example
 * --------------------
*/
export string SHORT_DATE_FORMAT = "%x";
/**
 * Short date format.
 *  Examples:
 * --------------------
 * auto dt = DateTime(2020, 11, 6, 23, 17, 42);
 * formatShortDateTime(dt); // Gives 06.11.2020 23:17:42 for example
 * --------------------
*/
export string SHORT_DATETIME_FORMAT = "%x %X";

/// Use system locale
static this()
{
    setlocale(LC_ALL, "");
}

/// Format date with current locale.
export string format(SysTime date, Locale locale = getDefaultLocale)
{
    return format(date, STANDARD_FORMAT, locale);
}

/// Format date with current locale without timezone.
export string format(DateTime date, Locale locale = getDefaultLocale)
{
    return format(cast(SysTime)date, STANDARD_FORMAT_WITHOUT_TIMEZONE, locale);
}

/// Format date with current locale without timezone.
export string format(Date date, Locale locale = getDefaultLocale)
{
    return format(cast(SysTime)date, STANDARD_DATE_FORMAT_WITHOUT_TIMEZONE, locale);
}

/// Get short date-only and current locale
export string formatShortDate(SysTime date, Locale locale = getDefaultLocale)
{
    return format(date, "%x", locale);
}

/// Get short date-only and current locale
export string formatShortDate(DateTime date, Locale locale = getDefaultLocale)
{
    return formatShortDate(cast(SysTime)date, locale);
}

/// Get short date-only and current locale
export string formatShortDate(Date date, Locale locale = getDefaultLocale)
{
    return format(cast(SysTime)date, "%x", locale);
}

/// Get short datetime and current locale
export string formatShortDateTime(SysTime date, Locale locale = getDefaultLocale)
{
    return format(date, "%x %X", locale);
}

/// Get short datetime and current locale
export string formatShortDateTime(DateTime date, Locale locale = getDefaultLocale)
{
    return formatShortDateTime(cast(SysTime)date, locale);
}

/** Format date with specific mask and current locale.
*   Params:
*       date = date to format
*       formatString = format string, like as for C strftime function.
*       locale = locale to format
*/
export string format(DateTime date, string formatString, Locale locale = getDefaultLocale)
{
    return format(cast(SysTime)date, formatString, locale);
}

/** Format date with specific mask and current locale.
*   Params:
*       date = date to format
*       formatString = format string, like as for C strftime function.
*       locale = locale to format
*/
export string format(Date date, string formatString, Locale locale = getDefaultLocale)
{
    return format(cast(SysTime)date, formatString, locale);
}

/** Format date with specific mask and current locale.
*    Params:
*       date = date to format
*       format = format string, like as for C strftime function.
*       locale = locale to format
*   Bugs:
*       some modifiers is not currenly supported: %G, %E, %O, %V, %+, %N
*		Windows - formats with no leading zero replaced by leading zero
*/
export string format(SysTime date, string format, Locale locale = getDefaultLocale)
{
    // Null date
    if (cast(DateTime)date == startOfTimes)
        return "";

    char[] result;
    while (format.length)
    {
        if (format[0] != '%')
        {
            result ~= format[0];
            format = format[1 .. $ ];
        } else
        {
            format = format[1 .. $ ];
            char ch = format[0];
            format = format[1 .. $ ];

            switch (ch)
            {
                case '%':
                    result ~= ch;
                    break;
                case 'a':
                    result ~= locale.abbrWeekDays[date.dayOfWeek];
                    break;
                case 'A':
                    result ~= locale.weekDays[date.dayOfWeek];
                    break;
                case 'b': 
                case 'h':
                    result ~= locale.abbrMonthes[date.month - 1];
                    break;
                case 'B':
                    result ~= locale.monthes[date.month - 1];
                    break;
                case 'c':
                    result ~= date.format(locale.dateTime, locale);
                    break;
                case 'C':
                    result ~= to!string(date.year)[ 0 .. $ - 2 ];
                    break;
                case 'd':
                    result ~= general(date.day);
                    break;
                case 'e':
                    result ~= spaced(date.day);
                    break;
                case 'D':
                    result ~= std.string.format("%s/%s/%d", general(date.month), general(date.day), date.year);
                    break;
                case 'F':
                    result ~= std.string.format("%d-%s-%s", date.year, general(date.month), general(date.day));
                    break;
                case 'g':
                    result ~= to!string(date.year)[ $ - 2 .. $ ]; 
                    break;
                case 'G':
                    result ~= to!string(date.year); 
                    break;
                case 'H':
                    result ~= general(date.hour); 
                    break;
                case 'I':
                    result ~= general(date.hour > 12 ? date.hour - 12 : date.hour); 
                    break;
                case 'j':
                    result ~= general(date.dayOfYear); 
                    break;
                case 'k':
                    result ~= spaced(date.hour); 
                    break;
                case 'l':
                    result ~= spaced(date.hour > 12 ? date.hour - 12 : date.hour); 
                    break;
                case 'm':
                    result ~= general(date.month); 
                    break;
                case 'M':
                    result ~= general(date.minute); 
                    break;
                case 'n':
                    result ~= '\n';
                    break;
                case 'p':
                    result ~= date.hour > 12 ? locale.pm : locale.am;
                    break;
                case 'P':
                    result ~= std.string.toLower(date.hour > 12 ? locale.pm : locale.am);
                    break;
                case 'q':
                    auto m = date.month;
                    result ~= to!string(m <= Month.mar ? 1 : (m > Month.mar && m <= Month.jun ? 2 : (m >= Month.oct ? 4 : 3)));
                    break;
                case 'r':
                    result ~= std.string.format("%s:%s:%s %s", 
                        general(date.hour > 12 ? date.hour - 12 : date.hour), 
                        general(date.minute), general(date.second),
                        date.hour > 12 ? locale.pm : locale.am);
                    break;
                case 'R':
                    result ~= general(date.hour) ~ ":" ~ general(date.minute);
                    break;
                case 's':
                    result ~= to!string(date.toUnixTime);
                    break;
                case 'S':
                    result ~= to!string(date.second);
                    break;
                case 't':
                    result ~= '\t';
                    break;
                case 'T':
                    result ~= general(date.hour) ~ ':' ~ general(date.minute) ~ ':' ~ general(date.second);
                    break;
                case 'u':
                    result ~= to!string(date.dayOfWeek == DayOfWeek.sun ? 7 : date.dayOfWeek); 
                    break;
                case 'U':
                    result ~= to!string(date.isoWeek);
                    break;
                case 'w':
                    result ~= to!string(cast(int)date.dayOfWeek);
                    break;
                case 'W':
                    result ~= to!string(date.isoWeek);
                    break;
                case 'x':
                    result ~= date.format(locale.date, locale);
                    break;
                case 'X':
                    result ~= date.format(locale.time, locale);
                    break;
                case 'y':
                    result ~= to!string(date.year)[ $ - 2 .. $];
                    break;
                case 'Y':
                    result ~= to!string(date.year);
                    break;
                case 'z':
                    int zHours, zMinutes;
                    date.utcOffset.split!("hours", "minutes")(zHours, zMinutes);
                    result ~= generalSigned(zHours) ~ general(zMinutes);
                    break;
                case 'Z':
                    result ~= date.timezone.stdName;
                    break;
                default:
                    result ~= "%" ~ ch;
            }
        }
    }

    return to!string(result);
}

/// Adds leading zero to one-digit natural numbers.
private string general(int arg)
{
    arg = abs(arg);
    return arg < 10 ? "0" ~ to!string(arg) : to!string(arg);
}

/// Adds leading zero to one-digit integers.
private string generalSigned(int arg)
{
    if (arg == 0)
        return "00";
    string result = (arg > 0 ? "+" : "") ~ to!string(arg);
    if (result.length < 3)
        result = result[ 0 .. 1 ] ~ "0" ~ result[ 1 .. $ ];
    return result;    
}


/// Adds leading space to one-digit natural numbers.
private string spaced(int arg)
{
    return arg < 10 ? " " ~ to!string(arg) : to!string(arg);
}

unittest
{
    import std.stdio;

    writeln("\nFormat test");
    writeln("Default system locale is " ~ getDefaultLocale.name);

    Locale locale;
    version(Posix)
        locale = initDateformatLocale("C");
    else
        version(Windows)
            locale = initDateformatLocale("en");
        else
            locale = initDateformatLocale("en-US");
    setDefaultLocale(locale.name);
    writeln("Test's locale is " ~ locale.name);

    // Test format routines
    SysTime date;
    assert(format(cast(SysTime)startOfTimes, "") == "");
    
    date = SysTime(DateTime(2020, 12, 7, 17, 45, 10), UTC());
    auto morning = SysTime(DateTime(2020, 12, 7, 9, 21, 14), UTC());

    assert(date.format("%%") == "%");
    
    assert(date.format("%a") == "Mon");
    assert(date.format("%A") == "Monday");
    assert(date.format("%a%A lala") == "MonMonday lala");

    assert(date.format("%b") == "Dec");
    assert(date.format("%h") == "Dec");
    assert(date.format("%B") == "December");

	version(Windows)
	{
		assert(date.format("%c") == "Monday, December 07, 2020 05:45");

        assert(date.format(locale) == "Monday, December 07, 2020 05:45 UTC");
        assert((cast(DateTime)date).format(locale) == "Monday, December 07, 2020 05:45");
        assert((cast(Date)date).format(locale) == "Monday, December 07, 2020");
        
        assert(date.formatShortDate(locale) == "12/07/2020");
        assert((cast(DateTime)date).formatShortDate(locale) == "12/07/2020");
        assert((cast(Date)date).formatShortDate(locale) == "12/07/2020");

        assert(date.formatShortDateTime(locale) == "12/07/2020 05:45");
        assert((cast(DateTime)date).formatShortDateTime(locale) == "12/07/2020 05:45");
		
		assert(morning.format("%c") == "Monday, December 07, 2020 09:21");
	}
	else
	{
		assert(date.format("%c") == "Mon Dec  7 17:45:10 2020");

        assert(date.format(locale) == "Mon Dec  7 17:45:10 2020 UTC");
        assert((cast(DateTime)date).format(locale) == "Mon Dec  7 17:45:10 2020");
        assert((cast(Date)date).format(locale) == "Mon 07 Dec 2020");
        
        assert(date.formatShortDate(locale) == "12/07/20");
        assert((cast(DateTime)date).formatShortDate(locale) == "12/07/20");
        assert((cast(Date)date).formatShortDate(locale) == "12/07/20");

        assert(date.formatShortDateTime(locale) == "12/07/20 17:45:10");
        assert((cast(DateTime)date).formatShortDateTime(locale) == "12/07/20 17:45:10");

		assert(morning.format("%c") == "Mon Dec  7 09:21:14 2020");
	}

    assert(date.format("%C") == "20");

    assert(date.format("%d") == "07");
    assert(date.format("%e") == " 7");

    assert(date.format("%D") == "12/07/2020");
    assert(date.format("%F") == "2020-12-07");

    assert(date.format("%g") == "20");
    assert(date.format("%G") == "2020");

    assert(date.format("%H") == "17");
    assert(date.format("%I") == "05");

    assert(date.format("%j") == "342");
    assert(date.format("%k") == "17");
    assert(morning.format("%k") == " 9");
    assert(date.format("%l") == " 5");

    assert(date.format("%m") == "12");

    assert(date.format("%M") == "45");
    
    assert(date.format("%n") == "\n");

    assert(date.format("%p") == "PM");
    assert(date.format("%P") == "pm");
    assert(morning.format("%p") == "AM");
    assert(morning.format("%P") == "am");

    assert(date.format("%q") == "4");
    auto moment = SysTime(DateTime(2020, 3, 8, 14, 33, 52), UTC());
    assert(moment.format("%q") == "1");
    moment.add!"months"(1);
    assert(moment.format("%q") == "2");
    moment.add!"months"(3);
    assert(moment.format("%q") == "3");

    assert(date.format("%r") == "05:45:10 PM");
    assert(morning.format("%r") == "09:21:14 AM");

    assert(date.format("%R") == "17:45");
    assert(morning.format("%R") == "09:21");

    assert(date.format("%s") == "1607363110");

    assert(date.format("%S") == "10");

    assert(date.format("%t") == "\t");

    assert(date.format("%T") == "17:45:10");
    assert(morning.format("%T") == "09:21:14");

    assert(date.format("%u") == "1");
    moment = SysTime(DateTime(2020, 3, 8, 14, 33, 52), UTC());
    assert(moment.format("%u") == "7");

    assert(moment.format("%U") == "10");
    assert(date.format("%U") == "50");
    
    assert(date.format("%w") == "1");
    assert(date.format("%W") == "50");

    version(Windows)
	{
		assert(date.format("%x") == "12/07/2020");
		assert(date.format("%X") == "05:45");
	}
	else
	{
		assert(date.format("%x") == "12/07/20");
		assert(date.format("%X") == "17:45:10");
	}

    assert(date.format("%y") == "20");
    moment = SysTime(DateTime(2012, 5, 16, 14, 14, 41), UTC());
    assert(moment.format("%y") == "12");
    assert(date.format("%Y") == "2020");
    assert(moment.format("%Y") == "2012");

    assert(date.format("%z") == "0000");
    assert(date.format("%Z") == "UTC");
}
